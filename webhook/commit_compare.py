"""Query MRs for upstream commit IDs to compare against submitted patches."""
import difflib
import enum
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
# GitPython
from git import Repo

from . import common

LOGGER = logger.get_logger('cki.webhook.commit_compare')
SESSION = session.get_session('cki.webhook.commit_compare')

READY_LABEL = ('All commits in this merge request have a suitable upstream or RHEL-only'
               ' commit reference.')
NEEDS_REVIEW_LABEL = ('This merge requests contains commits without sufficient upstream'
                      ' or RHEL-only attribution.')


def extract_ucid(message):
    """Extract upstream commit ID from the message."""
    message_lines = message.split('\n')
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gitlog_re = re.compile('^commit [a-f0-9]{40}$')
    # Pattern for 'git cherry-pick -x <commit>' based backports
    gitcherrypick_re = re.compile(r'^\(cherry picked from commit [a-f0-9]{40}\)$')
    # Bare hash matching pattern
    githash_re = re.compile('[a-f0-9]{40}', re.IGNORECASE)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile('^Upstream( Status)?:.*RHEL.*only', re.IGNORECASE)

    for line in message_lines:
        gitrefs = gitlog_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        gitrefs = gitcherrypick_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        if rhelonly_re.findall(line):
            gitref_list.append("RHELonly")
    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    return gitref_list


def noucid_msg():
    """Return the message to use as a note for commits without an upstream commit ID."""
    msg = "No commit information found.  All commits are required to have a 'commit: "
    msg += "<upstream_commit_hash>' entry, or an 'Upstream Status:' entry which explains why this "
    msg += "change is not upstream.  Please review this commit and add either entry.  Guidelines "
    msg += "for these entries can be found in CommitRules (URL:TBD)."
    return msg


def rhelonly_msg():
    """Return the message to use as note for commits that claim to be RHEL-only."""
    msg = "This commit has Upstream Status as RHEL-only and has no corresponding upstream commit.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def unk_cid_msg():
    """Return the message to use as note for commits that have an unknown commit ID."""
    msg = "No upstream source commit found.  This commit references an upstream commit ID, but "
    msg += "its source of origin is not recognized.  Please verify the upstream source tree."
    return msg


def diffs_msg():
    """Return the message to use as note for commits that differ from upstream."""
    msg = "This commit differs from the referenced upstream commit and should be evaluated "
    msg += "accordingly."
    return msg


def kabi_msg():
    """Return the message to use as note for commits that may contain kABI work-arounds."""
    msg = "This commit references a kABI work-around, and should be evaluated accordingly."
    return msg


def partial_msg():
    """Return the message to use as not for commits that match the part submitted."""
    msg = "This commit is a partial backport of the referenced upstream commit ID, and "
    msg += "the portions backported match upstream 100%."
    return msg


def badmail_msg():
    """Return the message to use for commits with invalid authorship."""
    msg = "This commit has invalid authorship. Commits must either have an authorship "
    msg += "email in the redhat.com domain, or they must match the email address of the "
    msg += "submitter of the merge request for outside contributors."
    return msg


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    BADMAIL = 6


def get_report_table(reviewed_items):
    """Create the table for the upstream commit ID report."""
    mr_approved = True
    table = []
    for cid in reviewed_items:
        ucid_properties = reviewed_items[cid]["validation"]
        mr_approved = mr_approved and ucid_properties['match'] != Match.NOUCID
        mr_approved = mr_approved and ucid_properties['match'] != Match.BADMAIL
        commits = []
        for commit in reviewed_items[cid]["commits"]:
            commits.append(commit)
        table.append([cid, commits, ucid_properties['match'],
                      ucid_properties['notes'], ucid_properties['logs']])
    return (table, mr_approved)


def get_match_info(match):
    """Get info on match type."""
    mstr = "No UCID   "
    full_match = False
    if match == Match.FULL:
        mstr = "100% match"
        full_match = True
    elif match == Match.PARTIAL:
        mstr = "Partial   "
    elif match == Match.DIFFS:
        mstr = "Diffs     "
    elif match == Match.KABI:
        mstr = "kABI Diffs"
    elif match == Match.RHELONLY:
        mstr = "n/a       "
    elif match == Match.BADMAIL:
        mstr = "Bad email "
    return (mstr, full_match)


def print_gitlab_report(notes, table, mr_approved):
    """Print an upstream commit ID mapping report for gitlab."""
    kerneloscope = "http://kerneloscope.usersys.redhat.com"
    full_match = True
    show_full_match_note = False
    evaluated_commits = 0
    report = "<br>\n\n**Upstream Commit ID Readiness Report**\n\n"
    report += "This report indicates how backported commits compare to the upstream source "
    report += "commit.  Matching (or not matching) is not a guarantee of correctness.  KABI, "
    report += "missing or un-backportable dependencies, or existing RHEL differences against "
    report += "upstream may lead a difference in commits. As always, care should be taken in "
    report += "the review to ensure code correctness.\n\n"
    report += "|UCID    |Sub CID |Match     |Notes   |\n"
    report += "|:-------|:-------|:---------|:-------|\n"
    for row in table:
        if row[2] == Match.FULL:
            show_full_match_note = True
            evaluated_commits += 1
            continue
        if row[0] != "-" and row[2] != Match.NOUCID and row[2] != Match.RHELONLY:
            ucidh = f"[{row[0][:8]}]({kerneloscope}/commit/{row[0]})"
        else:
            ucidh = f"{row[0][:8]}"
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        evaluated_commits += len(row[1])
        report += "|"+ucidh+"|"+"<br>".join(commits)
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += "Total number of commits analyzed: **%d** " % evaluated_commits
    if show_full_match_note:
        report += "(Patches that match upstream 100% not shown in table)"
    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request upstream commit IDs all present.\n"
        report += "Please note and evaluate differences from upstream in the table.\n"
    else:
        report += "\nThis Merge Request contains commits that are missing upstream commit ID "
        report += "references. Please review the table. \n"
        report += " \n"
        report += "To request re-evalution after resolving any issues with the commits in the "
        report += "merge request, add a comment to this MR with only the text:  "
        report += "request-commit-id-evaluation \n"

    return (report, full_match)


def print_text_report(notes, table, mr_approved):
    """Print an upstream commit ID mapping report for the text console."""
    full_match = True
    show_full_match_note = False
    evaluated_commits = 0
    report = "\n\nUpstream Commit ID Readiness Report\n"
    report += "* Patches that match upstream 100% not shown\n\n"
    report += "|UCID    |Sub CID |Match     |Notes   |\n"
    report += "|:-------|:-------|:---------|:-------|\n"
    for row in table:
        if row[2] == Match.FULL:
            show_full_match_note = True
            evaluated_commits += 1
            continue
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0][:8])+"|"+" ".join(commits)
        evaluated_commits += len(row[1])
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += "Merge Request %s upstream commit ID validation.\n" % status
    if mr_approved:
        report += "Please verify differences from upstream.\n"
    report += "Total number of commits analyzed: **%d** " % evaluated_commits
    if show_full_match_note:
        report += "(Patches that match upstream 100% not shown in table)"
    return (report, full_match)


def build_review_lists(mri, ucid, review_lists, cid):
    """Build review_lists for this commit ID."""
    try:
        found_files = common.extract_files(mri.project.commits.get(cid))
    # pylint: disable=broad-except
    except Exception:
        found_files = []

    cid_data = review_lists[cid] if cid in review_lists else {}
    commits = cid_data["commits"] if "commits" in cid_data else {}
    commits[ucid] = found_files
    cid_data["commits"] = commits
    return cid_data


def search_compiled(line, re_list, group0=False, first=True):
    """Actual regex matching happens here."""
    matches = []
    for item in re_list:
        match = item.search(line)
        if match:
            if group0 and match.group(0):
                matches.append(match.group(0))
            elif not group0 and match.group(1):
                matches.append(match.group(1))
            if first:
                break

    return matches


def filter_diff(diff):
    """Filter the diff, isolating only the kinds of differences we care about."""
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_header = True
    hit = False
    header = []
    new_diff = []
    cache = []
    diff_compiled = []

    # sort through rules to see if this chunk is worth saving
    # most of the filters are patch header junk.  Don't care if those
    # offsets are different.
    # The last filter is the most interesting: filter context differences
    # Basically that just filters out noise that changed around
    # the patch and not the parts of the patch that does anything.
    # IOW, the lines in a patch with _no_ ± in front.
    # Personal preference if that is interesting or not.

    diff_re = ["^[+|-]index ",
               "^[+|-]--- ",
               r"^[+|-]\+\+\+",
               "^[+|-]diff ",
               "^[+|-]$",
               "^[+|-]@@ ",
               "^[+|-] "]

    for prefix in diff_re:
        diff_compiled.append(re.compile(prefix))

    # end pre-compile stuff

    for line in diff:

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # filter lines
        if line[0] == " ":
            # patch context, ignore
            continue
        if search_compiled(line, diff_compiled, group0=True):
            # hit junk, skip
            continue

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    return new_diff


def compare_commit_to_upstream(udiff, cdiff):
    """Perform interdiff on the upstream and submitted patches' diffs."""
    interdiff = difflib.unified_diff(udiff.split('\n'), cdiff.split('\n'),
                                     fromfile='upstream', tofile='submission', lineterm="")
    interesting = filter_diff(interdiff)
    return interesting


def get_partial_diff(diff, filelist):
    """Extract partial diff, used for partial backport comparisons."""
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_wanted_file = False
    in_header = True
    hit = False
    partial_diff = ""
    header = []
    new_diff = []
    cache = []

    for line in diff.split('\n'):
        if line.startswith("diff --git "):
            in_wanted_file = False
            for path in filelist:
                if path in line:
                    in_wanted_file = True
                    continue

        if not in_wanted_file:
            continue

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    for line in new_diff:
        partial_diff += "%s\n" % line

    return partial_diff


def get_upstream_diff(mri, ucid, filelist):
    """Extract diff for upstream commit ID, optionally limiting to filelist."""
    repo = Repo(mri.linux_src)

    try:
        commit = repo.commit(ucid)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.debug("Commit ID %s not found in upstream", ucid)
        return None

    diff = repo.git.diff("%s^" % commit, commit, '--no-renames')
    # author_name = commit.author.name
    author_email = commit.author.email

    if len(filelist) == 0:
        return (diff, author_email)

    LOGGER.debug("Getting partial diff for %s", ucid)
    part_diff = get_partial_diff(diff, filelist)

    return (part_diff, author_email)


def get_submitted_diff(commit):
    """Extract diff for submitted patch."""
    diff = ""
    filelist = []

    for path in commit:
        diff += "diff --git a/" + path['old_path'] + " "
        diff += "b/" + path['new_path'] + "\n"
        diff += "index blahblah..blahblah 100644\n"
        diff += "--- a/" + path['old_path'] + "\n"
        diff += "+++ b/" + path['new_path'] + "\n"
        diff += path['diff']
        if path['old_path'] not in filelist:
            filelist.append(path['old_path'])
        if path['new_path'] not in filelist:
            filelist.append(path['new_path'])

    return (diff, filelist)


def find_kabi_hints(diff):
    """Check for hints this patch has kABI workarounds in it."""
    if "genksyms" in diff.lower() or "rh_kabi" in diff.lower():
        return True
    return False


def process_no_ucid_commit(mri):
    """Process a commit w/no associated upstream commit ID."""
    mri.notes += [] if mri.noucid_id else [noucid_msg()]
    mri.noucid_id = mri.noucid_id if mri.noucid_id else str(len(mri.notes))
    ucid_properties = {'match': Match.NOUCID, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.noucid_id)
    return ucid_properties


def process_rhel_only_commit(mri):
    """Process a commit listed as being RHEL-only."""
    mri.notes += [] if mri.rhelonly_id else [rhelonly_msg()]
    mri.rhelonly_id = mri.rhelonly_id if mri.rhelonly_id else str(len(mri.notes))
    ucid_properties = {'match': Match.RHELONLY, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.rhelonly_id)
    return ucid_properties


def process_unknown_commit(mri):
    """Process a commit with an unrecognized commit ID ref."""
    mri.notes += [] if mri.unknown_id else [unk_cid_msg()]
    mri.unknown_id = mri.unknown_id if mri.unknown_id else str(len(mri.notes))
    ucid_properties = {'match': Match.NOUCID, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.unknown_id)
    return ucid_properties


def process_partial_backport(mri):
    """Process a commit that is a partial backport of an upstream commit."""
    mri.notes += [] if mri.part_id else [partial_msg()]
    mri.part_id = mri.part_id if mri.part_id else str(len(mri.notes))
    ucid_properties = {'match': Match.PARTIAL, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.part_id)
    return ucid_properties


def process_kabi_patch(mri):
    """Process a commit that references kABI workarounds."""
    mri.notes += [] if mri.kabi_id else [kabi_msg()]
    mri.kabi_id = mri.kabi_id if mri.kabi_id else str(len(mri.notes))
    ucid_properties = {'match': Match.KABI, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.kabi_id)
    return ucid_properties


def process_commit_with_diffs(mri):
    """Process a commit with differences from upstream."""
    mri.notes += [] if mri.diffs_id else [diffs_msg()]
    mri.diffs_id = mri.diffs_id if mri.diffs_id else str(len(mri.notes))
    ucid_properties = {'match': Match.DIFFS, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.diffs_id)
    return ucid_properties


def process_invalid_author_commit(mri):
    """Process a commit with invalid authorship."""
    mri.notes += [] if mri.badmail_id else [badmail_msg()]
    mri.badmail_id = mri.badmail_id if mri.badmail_id else str(len(mri.notes))
    ucid_properties = {'match': Match.BADMAIL, 'notes': [], 'logs': ''}
    ucid_properties['notes'].append(mri.badmail_id)
    return ucid_properties


def valid_rhcommit_author(rhcommit_email, upstream_email, mr_email):
    """Attempt to make sure we have proper authorship on submitted commits."""
    # if the commit has a redhat.com author, it's valid
    if rhcommit_email.split('@')[1] == 'redhat.com':
        return True

    # if the commit author email matches the upstream email, but not the MR submitter email,
    # then they probably cherry-picked without a follow-up git commit -a --reset-author
    if rhcommit_email == upstream_email and rhcommit_email != mr_email:
        LOGGER.debug("Email on commit doesn't match MR submitter email")
        return False

    # if the commit author and MR submitter emails match, probably an outside contributor
    if rhcommit_email == mr_email:
        return True

    # anything else... let's flag it invalid.
    return False


def validate_commit_ids(mri, review_lists):
    """Iterate through the upstream commit IDs we found and compare w/our submitted commits."""
    idiff = ""

    for ucid in review_lists:
        if ucid == "-":
            review_lists[ucid]["validation"] = process_no_ucid_commit(mri)
            continue

        if ucid == "RHELonly":
            review_lists[ucid]["validation"] = process_rhel_only_commit(mri)
            continue

        upstream = get_upstream_diff(mri, ucid, [])
        if upstream is None:
            review_lists[ucid]["validation"] = process_unknown_commit(mri)
            continue

        for cid in review_lists[ucid]["commits"]:
            rhcommit = mri.project.commits.get(cid)
            mydiff = get_submitted_diff(rhcommit.diff())
            rhcommit_email = rhcommit.author_email
            idiff = compare_commit_to_upstream(upstream[0], mydiff[0])

        if not valid_rhcommit_author(rhcommit_email, upstream[1], mri.submitter_email):
            review_lists[ucid]["validation"] = process_invalid_author_commit(mri)
            continue

        if len(idiff) == 0:
            review_lists[ucid]["validation"] = {'match': Match.FULL, 'notes': [], 'logs': ''}
        else:
            upstream_partial = get_upstream_diff(mri, ucid, mydiff[1])
            idiff = compare_commit_to_upstream(upstream_partial[0], mydiff[0])
            if len(idiff) == 0:
                review_lists[ucid]["validation"] = process_partial_backport(mri)
            elif find_kabi_hints(mydiff[0]):
                review_lists[ucid]["validation"] = process_kabi_patch(mri)
            else:
                review_lists[ucid]["validation"] = process_commit_with_diffs(mri)


def check_on_ucids(mri):
    """Check upstream commit IDs."""
    review_lists = {}
    for commit in mri.merge_request.commits():
        try:
            found_refs = extract_ucid(commit.message)
        # pylint: disable=broad-except
        except Exception:
            found_refs = []

        for ucid in found_refs:
            review_lists[ucid] = build_review_lists(mri, commit.id,
                                                    review_lists, ucid)

        if not found_refs:
            review_lists["-"] = build_review_lists(mri, commit.id,
                                                   review_lists, "-")

    validate_commit_ids(mri, review_lists)
    (table, approved) = get_report_table(review_lists)
    return (table, approved)


# pylint: disable=too-many-instance-attributes,too-few-public-methods
class MRUCIDInstance:
    """Merge request Instance."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the commit ID comparison instance."""
        self.linux_src = "/usr/src/linux"
        self.lab = lab
        self.log_ok_scope = False
        self.project = project
        self.merge_request = merge_request
        self.submitter_email = ""
        self.payload = payload
        self.notes = []
        self.noucid_id = ""
        self.rhelonly_id = ""
        self.unknown_id = ""
        self.diffs_id = ""
        self.kabi_id = ""
        self.part_id = ""
        self.badmail_id = ""

    def run_ucid_validation(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.info("Running upstream commit ID validation")
        LOGGER.debug("Merge request description:\n%s", self.merge_request.description)
        (table, approved) = check_on_ucids(self)
        # append quick label "Commitrefs" with scope "OK" or "NeedsReview"
        if approved:
            status = "OK"
            label_color = common.READY_LABEL_COLOR
            label_description = READY_LABEL
        else:
            status = "NeedsReview"
            label_color = common.NEEDS_REVIEW_LABEL_COLOR
            label_description = NEEDS_REVIEW_LABEL
        label = common.create_label_object(f'CommitRefs::{status}', label_color, label_description)
        common.add_label_to_merge_request(self.project, self.merge_request.iid, [label])
        if misc.is_production():
            (report, full_match) = print_gitlab_report(self.notes, table, approved)
            if not full_match:
                self.merge_request.notes.create({'body': report})
        else:
            LOGGER.info('Skipping adding report in non-production')
            (report, full_match) = print_text_report(self.notes, table, approved)
            if not full_match:
                LOGGER.info(report)
            else:
                LOGGER.info('Patches all match upstream 100%')
        if not approved:
            LOGGER.info("Some commits require further manual inspection!")


def get_mri(gl_instance, message, key):
    """Return a merge request instance for the webhook payload."""
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(message.payload[key]["iid"])
    return MRUCIDInstance(gl_instance, gl_project, gl_mergerequest, message.payload)


def perform_mri_tasks(mri, linux_src, label_changed):
    """Perform tasks if mri is valid."""
    if mri:
        mri.submitter_email = str(mri.lab.users.get(mri.merge_request.author['id']).public_email)
        mri.linux_src = linux_src
        mri.log_ok_scope = label_changed
        mri.run_ucid_validation()


def process_mr(gl_instance, message, linux_src):
    """Process a merge request message."""
    label_changed = common.has_label_changed(message.payload, 'CommitRefs::', True)
    if not common.mr_action_affects_commits(message) and not label_changed:
        return

    mri = get_mri(gl_instance, message, "object_attributes")
    perform_mri_tasks(mri, linux_src, label_changed)


def process_note(gl_instance, message, linux_src):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    mri = get_mri(gl_instance, message, "merge_request")
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'],
                                           'commit-id'):
        return

    perform_mri_tasks(mri, linux_src, True)


WEBHOOKS = {
    'merge_request': process_mr,
    'note': process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src)


if __name__ == "__main__":
    main(sys.argv[1:])
