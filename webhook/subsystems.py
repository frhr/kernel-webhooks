"""Add kernel subsystem topics as labels to an MR."""
import sys

from cki_lib import logger
from cki_lib import session
from yaml import safe_load

from . import common

LOGGER = logger.get_logger('cki.webhook.subsystems')
SESSION = session.get_session('cki.webhook.subsystems')

SUBSYS_LABEL_PREFIX = 'Subsystem'
SUBSYS_LABEL_COLOR = '#778899'
SUBSYS_LABEL_DESC = 'An MR that affects code related to %s.'
DRIVER_LABEL_PREFIX = 'Driver'
DRIVER_LABEL_COLOR = '#2f4f4f'
DRIVER_LABEL_DESC = 'An MR that affects code related to the %s driver.'


def load_yaml_data(map_file):
    """Load the yaml map file."""
    try:
        with open(map_file) as yaml_file:
            return safe_load(yaml_file)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Problem loading yaml file.')
        return None


def get_subsystems(path, mapping_data):
    """Return a list of subsystem(s) topics that match the path, if any."""
    topics_list = []
    for subsystem in mapping_data:
        if path.startswith(tuple(mapping_data[subsystem])):
            topics_list.append(subsystem)
    return topics_list


def make_labels(topics):
    """Given a list of topics return a list of gitlab label objects."""
    label_list = []
    for topic in topics:
        if topic.startswith('driver '):
            driver_name = topic.split()[1]
            label_color = DRIVER_LABEL_COLOR
            label_description = DRIVER_LABEL_DESC % driver_name
            label = common.create_label_object(f'{DRIVER_LABEL_PREFIX}:{driver_name}',
                                               label_color, label_description)
        else:
            label_color = SUBSYS_LABEL_COLOR
            label_description = SUBSYS_LABEL_DESC % topic
            label = common.create_label_object(f'{SUBSYS_LABEL_PREFIX}:{topic}',
                                               label_color, label_description)
        label_list.append(label)
    return label_list


def do_mapping(merge_request, mapping_data):
    """Generate a list of GL label dicts based upon the paths in the given MR and map_file."""
    topic_set = set()
    path_set = set()
    for commit in merge_request.commits():
        path_set.update(common.extract_files(commit))

    for path in path_set:
        topics = get_subsystems(path, mapping_data)
        if topics:
            LOGGER.debug("Path '%s' matched topics: %s.", path, topics)
            topic_set.update(topics)
        else:
            LOGGER.debug("No matching subsystem topics found for path '%s'.", path)

    if not topic_set:
        LOGGER.debug('No matching subsystem topics found for MR file list: %s.', path_set)
        return None

    label_list = make_labels(topic_set)
    return label_list


def _do_process_mr(gl_instance, message, mr_id, map_file):
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    mapping_data = load_yaml_data(map_file)
    if not mapping_data:
        LOGGER.error("Mapping data not loaded from '%s'.", map_file)
        return

    matched_labels = do_mapping(gl_mergerequest, mapping_data)
    if not matched_labels:
        LOGGER.info('No labels to add.')
        return

    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, matched_labels)


def process_mr(gl_instance, msg, map_file):
    """Process a merge request message."""
    # If the MR file contents haven't changed and our labels haven't changed then don't run.
    commits_changed = common.mr_action_affects_commits(msg)
    subsys_label_changed = common.has_label_changed(msg.payload, f'{SUBSYS_LABEL_PREFIX}:', True)
    driver_label_changed = common.has_label_changed(msg.payload, f'{DRIVER_LABEL_PREFIX}:', True)
    if not commits_changed and not subsys_label_changed and not driver_label_changed:
        return
    _do_process_mr(gl_instance, msg, msg.payload["object_attributes"]["iid"], map_file)


WEBHOOKS = {
    "merge_request": process_mr,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SUBSYSTEMS')
    parser.add_argument('--map-file', **common.get_argparse_environ_opts('MAP_FILE'),
                        help='Yaml file containing mapping of kernel paths to subsystems')
    args = parser.parse_args(args)
    LOGGER.info('Using map file: %s', args.map_file)
    common.generic_loop(args, WEBHOOKS, map_file=args.map_file)


if __name__ == "__main__":
    main(sys.argv[1:])
