"""Query bugzilla for MRs."""
import json
import os
import re
import sys

import bugzilla
from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import common

LOGGER = logger.get_logger('cki.webhook.bugzilla')
SESSION = session.get_session('cki.webhook.bugzilla')

BZ_READY_LABEL = "This merge request's bugzillas are approved for integration."
BZ_NEEDS_REVIEW_LABEL = ('This merge request has bugzillas associated with it that are not yet'
                         ' approved for integration, or commits without a valid bug'
                         ' referenced in them.')
DEP_READY_LABEL = ("This merge request does not contain any dependencies that aren't already"
                   " merged into the main tree.")
DEP_NEEDS_REVIEW_LABEL = ('This merge request depends on code which does not exist in the'
                          'target branch.')


def try_bugzilla_conn():
    """If BUGZILLA_API_KEY is set then try to return a bugzilla connection object."""
    if not os.environ.get('BUGZILLA_API_KEY'):
        LOGGER.info("No bugzilla API key, not connecting to bugzilla.")
        return False
    return connect_bugzilla(os.environ.get('BUGZILLA_API_KEY'))


def connect_bugzilla(api_key, cookie_file=None, token_file=None):
    """Connect to bugzilla and return a bugzilla connection object."""
    try:
        # See https://github.com/python-bugzilla/python-bugzilla/blob/master/bugzilla/base.py#L175
        bzcon = bugzilla.Bugzilla('bugzilla.redhat.com', api_key=api_key,
                                  cookiefile=cookie_file, tokenfile=token_file)
    except ConnectionError:
        LOGGER.exception("Problem connecting to bugzilla server.")
        return False
    except PermissionError:
        LOGGER.exception("Problem with file permissions for bugzilla connection.")
        return False
    return bzcon


def update_bugzilla(project, bugs, merge_request):
    """Filter input bug list and run bugzilla API actions."""
    if not bugs:
        LOGGER.debug('Input bug list is empty.')
        return
    # Ignore non-numeric bugs such as 'INTERNAL'. Bug list items are strings :/.
    bugs_to_exclude = list(filter(lambda bug: not bug.isdigit(), bugs))
    if bugs_to_exclude:
        LOGGER.info('Excluding these "bugs" from bugzilla actions: %s', bugs_to_exclude)
        if bugs_to_exclude == bugs:
            return
    bugs_filtered = [item for item in bugs if item not in bugs_to_exclude]
    bzcon = try_bugzilla_conn()
    if bzcon:
        add_mr_to_bz(bzcon, project, bugs_filtered, merge_request.iid)


def bz_is_linked_to_mr(bug, domain, path):
    """Return True if any of the bug's trackers point to the given MR."""
    if not bug.external_bugs:
        return False
    for tracker in bug.external_bugs:
        if tracker['type']['description'] != "Gitlab":
            continue
        if tracker['type']['url'].split('/')[2] == domain and \
           tracker['ext_bz_bug_id'] == path:
            return True
    return False


def add_mr_to_bz(bzcon, project, bug_list, mr_id):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    if not bug_list:
        LOGGER.error("MR %d has no bugs? Skipping linking of bugs to MR.", mr_id)
        return

    bz_results = bzcon.getbugs(bug_list)
    if not bz_results:
        LOGGER.error("getbugs() returned an empty list for %s.", bug_list)
        return

    # Properties that we want to match when checking a BZ's trackers
    mr_domain = project.web_url.split('/')[2]
    mr_path = f"{project.path_with_namespace}/-/merge_requests/{mr_id}"

    untracked_bugs = list(filter(lambda bug: not bz_is_linked_to_mr(bug, mr_domain, mr_path),
                                 bz_results))
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to MR %d.", mr_id)
        return

    new_bugs = list(map(lambda bug: bug.id, untracked_bugs))
    LOGGER.info("Need to add MR %d to external tracker list of these bugs: %s", mr_id, new_bugs)
    if misc.is_production():
        ext_domain = f"https://{mr_domain}/"
        try:
            bzcon.add_external_tracker(new_bugs, ext_type_url=ext_domain, ext_bz_bug_id=mr_path)
        # pylint: disable=broad-except
        except Exception:
            LOGGER.exception("Problem adding tracker %s%s to BZs.", ext_domain, mr_path)


def find_bz_in_line(line, prefix):
    """Return bug number from properly formated Bugzilla: line."""
    # BZs must be called out one-per-line, begin with 'Bugzilla:' and contain a complete BZ URL.
    # Plus an exception for INTERNALs.
    line = line.rstrip()
    pattern = prefix + r': http(s)?://bugzilla\.redhat\.com/(show_bug\.cgi\?id=)?(?P<bug>\d{4,8})$'
    bznum_re = re.compile(pattern)
    bugs = bznum_re.match(line)
    if bugs:
        return bugs.group('bug')
    if line == prefix + ': INTERNAL':
        return 'INTERNAL'
    return None


def extract_all_bzs(message, mr_bugs, dependencies):
    """Extract all BZs from the message."""
    bzs = []
    non_mr_bzs = []
    dep_bzs = []

    if message:
        mlines = message.splitlines()
        for line in mlines:
            bug = find_bz_in_line(line, 'Bugzilla')
            if not bug:
                continue
            if bug in dependencies:
                dep_bzs.append(bug)
            elif mr_bugs and bug not in mr_bugs:
                LOGGER.debug("Bugzilla: %s not listed in MR description.", bug)
                non_mr_bzs.append(bug)
            else:
                bzs.append(bug)
    # We return empty arrays if no bugs are found
    return (bzs, non_mr_bzs, dep_bzs)


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y = extract_all_bzs(message, [], [])
    return bzs


def extract_dependencies(description):
    """Extract Depends: bugs from MR description."""
    bzs = []

    if description:
        dlines = description.splitlines()
        for line in dlines:
            bug = find_bz_in_line(line, 'Depends')
            if not bug:
                continue
            bzs.append(bug)
    # We return an empty array if there are no bz deps
    return bzs


def get_target_branch(project, merge_request):
    """Determine the target branch from merge request."""
    # Check what rhel minor branch the merge request is for. In case of the
    # main branch, it is the latest released minor + 1, we calculate it
    # based on the branches available in the repository. We use the minor
    # number to know which dist-git repository is our target for checking.
    major = "\\d+"
    glab_search = None
    basename = project.namespace["name"]
    match = re.search(r'^(\d+)\.', basename)
    if match:
        major = match.group(1)
        glab_search = "^%s." % (major)
    else:
        LOGGER.debug("validate_bzs: unable to get the major version")
    sre = f"^({re.escape(major)})\\.(\\d+)"
    major = 0
    minor = 0
    valid_target = False
    if merge_request.target_branch == "main":
        branches = project.branches.list(all=True, search=glab_search)
        bmain = -1
        sre += "$"
        regex = re.compile(sre)
        for branch in branches:
            match = regex.search(branch.name)
            if match:
                tmp = int(match.group(2))
                if tmp > bmain:
                    bmain = tmp
                    major = int(match.group(1))
                    valid_target = True
        minor = bmain + 1
    else:
        regex = re.compile(sre)
        match = regex.search(merge_request.target_branch)
        if match:
            major = int(match.group(1))
            minor = int(match.group(2))
            valid_target = True
        else:
            LOGGER.debug("validate_bzs: target branch not recognized")
    target_branch = f"rhel-{major}.{minor}.0"
    return (valid_target, target_branch)


def nobug_msg():
    """Return the message to use as a note for commits without bz."""
    msg = "No 'Bugzilla:' entry was found in these commits.  This project requires that each "
    msg += "commit have at least one 'Bugzilla:' entry.  Please add a 'Bugzilla: <bugzilla_URL>' "
    msg += "entry for each bugzilla.  Guidelines for 'Bugzilla:' entries can be found in "
    msg += "CommitRules (URL:TBD)."
    return msg


def depbug_msg():
    """Return the message to use as note for commits that are dependencies."""
    msg = "These commits are for dependencies listed in the MR description.  Your MR will not be "
    msg += "merged until these dependencies are in main."
    return msg


def nomrbug_msg():
    """Return the message to note an unknown bug found in patches."""
    msg = "These commits contain a bugzilla number that was not listed in the merge request "
    msg += "description.  Please verify the bugzilla's URL is correct, or, add them to your MR "
    msg += "description as either a 'Bugzilla:' or a 'Depends:' entry.  Guidelines for these "
    msg += "entries can be found in CommitRules (URL:TBD)."
    return msg


def badinternalbug_msg():
    """Return the message that 'INTERNAL' bug touches wrong source files."""
    msg = "These commits are marked as 'INTERNAL' but were found to touch "
    msg += "source files outside of the redhat/ directory. 'INTERNAL' bugs "
    msg += "are only to be used for changes to files in the redhat/ directory."
    return msg


def validate_internal_bz(commit_list):
    """Check file list of 'INTERNAL' bug only touches redhat/ files."""
    for commit in commit_list.values():
        for path in commit:
            if not path.startswith('redhat/'):
                return False
    return True


# pylint: disable=too-many-locals,too-many-branches,too-many-statements
def validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, reviewed_items):
    """Validate bugzilla references."""
    (valid_target, target_branch) = get_target_branch(project, merge_request)
    notes = []
    nobug_id = ""
    depbug_id = ""
    nomrbug_id = ""
    notgt_id = ""
    internalbug_id = ""
    for bug in reviewed_items:
        bz_properties = {'approved': False, 'notes': [], 'logs': ''}
        reviewed_items[bug]["validation"] = bz_properties

        if not valid_target:
            notes += [] if notgt_id else ["Unknown repository target"]
            notgt_id = notgt_id if notgt_id else str(len(notes))
            bz_properties['notes'].append(notgt_id)
            continue

        if bug == 'INTERNAL':
            if validate_internal_bz(reviewed_items[bug]['commits']):
                bz_properties['approved'] = True
            else:
                notes += [] if internalbug_id else [badinternalbug_msg()]
                internalbug_id = internalbug_id if internalbug_id else str(len(notes))
                bz_properties['notes'].append(internalbug_id)
            continue

        if bug == "-":
            notes += [] if nobug_id else [nobug_msg()]
            nobug_id = nobug_id if nobug_id else str(len(notes))
            bz_properties['notes'].append(nobug_id)
            continue

        if bug in bz_dependencies:
            notes += [] if depbug_id else [depbug_msg()]
            depbug_id = depbug_id if depbug_id else str(len(notes))
            bz_properties['notes'].append(depbug_id)

        elif bug not in bugzilla_ids:
            notes += [] if nomrbug_id else [nomrbug_msg()]
            nomrbug_id = nomrbug_id if nomrbug_id else str(len(notes))
            bz_properties['notes'].append(nomrbug_id)

        buginputs = {
            'package': 'kernel',
            'namespace': 'rpms',
            'ref': 'refs/heads/' + target_branch,
            'commits': [{
                'hexsha': 'HEAD',
                'files': ["kernel.spec"],
                'resolved': [int(bug)],
                'related': [],
                'reverted': [],
            }]
        }
        print(json.dumps(buginputs, indent=2))
        # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
        bugresults = SESSION.post(
            "https://dist-git.host.prod.eng.bos.redhat.com"
            "/lookaside/gitbz-query.cgi",
            json=buginputs)

        resjson = bugresults.json()
        print(json.dumps(resjson, indent=2))
        if resjson['result'] == 'ok':
            bz_properties['approved'] = True
        else:
            bz_properties['approved'] = False
            noteid = 0
            while noteid < len(notes):
                if notes[noteid] == resjson['error']:
                    break
                noteid += 1
            if noteid == len(notes):
                notes.append(resjson['error'])
            bz_properties['notes'].append(str(noteid+1))
    return (notes, target_branch)


def get_report_table(reviewed_items):
    """Create the table for the bugzilla report."""
    mr_approved = True
    table = []
    for bug in reviewed_items:
        bz_properties = reviewed_items[bug]["validation"]
        mr_approved = mr_approved and bz_properties['approved']
        commits = []
        for commit in reviewed_items[bug]["commits"]:
            commits.append(commit)
        table.append([bug, commits, bz_properties['approved'],
                      bz_properties['notes'], bz_properties['logs']])
    return (table, mr_approved)


def trim_notes(notes):
    """Trim/remove unwanted notes returned from dist-git cgi script."""
    noteid = 0
    while noteid < len(notes):
        trimmed = ""
        if notes[noteid] is None:
            noteid += 1
            continue
        strings = notes[noteid].split('\n')
        for string in strings:
            drop = False
            drop = string.startswith("*** eg: Resolves: rhbz#") or drop
            drop = string.startswith("*** Make sure to follow the") or drop
            drop = string.startswith("*** No issue IDs referenced") or drop
            drop = string.startswith("*** No approved issue") or drop
            drop = string.startswith("*** Unapproved issue") or drop
            drop = string.startswith("*** Commit") or drop
            trimmed += "" if drop else string + '\n'
        notes[noteid] = trimmed
        noteid += 1


def print_target(merge_request, bz_target):
    """Print the bugzilla target if MR targets main branch."""
    target = merge_request.target_branch
    target = target if target != "main" else f"main ({bz_target})"
    return "Target Branch: " + target


def print_gitlab_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for gitlab."""
    report = "<br>\n\n**Bugzilla Readiness Report**\n\n"
    report += print_target(merge_request, bz_target) + "   \n"
    report += " \n\n"
    report += "|BZ|Commits|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for row in table:
        bzn = f"BZ-{row[0]}" if row[0] != "-" else row[0]
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        report += "|"+bzn+"|"+"<br>".join(commits)
        report += "|"+str(row[2])+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request passes bugzilla validation\n"
    else:
        report += "\nMerge Request fails bugzilla validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bugzilla approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    return report


def print_text_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for the text console."""
    report = "\n\nBZ Readiness Report\n\n"
    report += print_target(merge_request, bz_target) + "\n\n"
    for row in table:
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0])+"|"+" ".join(commits)
        report += "|"+str(row[2])+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += "Merge Request %s bugzilla validation\n" % status
    return report


def build_review_lists(commit, review_lists, bug):
    """Build review_lists for this bug."""
    found_files = []
    if bug == 'INTERNAL':
        found_files = common.extract_files(commit)

    bugd = review_lists[bug] if bug in review_lists else {}
    commits = bugd["commits"] if "commits" in bugd else {}
    commits[commit.id] = found_files
    bugd["commits"] = commits
    return bugd


def check_on_bzs(project, merge_request, bugzilla_ids, bz_dependencies):
    """Check BZs."""
    review_lists = {}
    mrdeps = False
    depcommit = "nada"
    for commit in merge_request.commits():
        commit = project.commits.get(commit.id)
        # Skip merge commits, bugzilla link on the commit log for them
        # is not required and we do not care if bugs are listed on them
        if len(commit.parent_ids) > 1:
            continue
        try:
            found_bzs, non_mr_bzs, dep_bzs = extract_all_bzs(commit.message,
                                                             bugzilla_ids,
                                                             bz_dependencies)
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
            non_mr_bzs = []

        for bug in found_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in non_mr_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in dep_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)
            if not mrdeps:
                depcommit = commit.id
            mrdeps = True

        if not found_bzs and not non_mr_bzs and not dep_bzs:
            review_lists["-"] = build_review_lists(commit, review_lists, "-")

    (notes, tgt) = validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, review_lists)
    (table, approved) = get_report_table(review_lists)
    trim_notes(notes)

    return (notes, tgt, table, approved, mrdeps, depcommit)


def get_dependencies_label_scope(mrdeps, depcommit):
    """Set Dependencies:: label with scope of either OK or the sha of first dependent commit."""
    if mrdeps:
        scope = depcommit[:12]
    else:
        scope = "OK"

    return scope


def run_bz_validation(project, merge_request, force_show_summary):
    """Perform the Bugzilla validation for a merge request."""
    bugzilla_ids = extract_bzs(merge_request.description)
    bz_dependencies = extract_dependencies(merge_request.description)
    LOGGER.info("Running BZ validation for merge request: iid=%s, description=%s, bz=%s, deps=%s",
                merge_request.iid, merge_request.description, bugzilla_ids, bz_dependencies)

    (notes, bz_target, table, approved, mrdeps, depcommit) = check_on_bzs(project, merge_request,
                                                                          bugzilla_ids,
                                                                          bz_dependencies)

    if approved:
        status = 'OK'
        label_color = common.READY_LABEL_COLOR
        label_description = BZ_READY_LABEL
    else:
        status = 'NeedsReview'
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        label_description = BZ_NEEDS_REVIEW_LABEL
    label = common.create_label_object(f'Bugzilla::{status}', label_color, label_description)
    label_changed = common.add_label_to_merge_request(project, merge_request.iid, [label])

    LOGGER.debug(print_text_report(merge_request, notes, bz_target, table, approved))

    if misc.is_production() and ((label_changed and not approved) or force_show_summary):
        report = print_gitlab_report(merge_request, notes, bz_target, table, approved)
        merge_request.notes.create({'body': report})

    dep_scope = get_dependencies_label_scope(mrdeps, depcommit)
    if dep_scope == 'OK':
        label_color = common.READY_LABEL_COLOR
        label_description = DEP_READY_LABEL
    else:
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        label_description = DEP_NEEDS_REVIEW_LABEL
    label = common.create_label_object(f'Dependencies::{dep_scope}', label_color,
                                       label_description)
    common.add_label_to_merge_request(project, merge_request.iid, [label])
    update_bugzilla(project, bugzilla_ids + bz_dependencies, merge_request)


def bz_process_mr(gl_instance, message):
    """Process a merge request message."""
    project = gl_instance.projects.get(message.payload["project"]["id"])
    merge_request = project.mergerequests.get(message.payload["object_attributes"]["iid"])
    run_bz_validation(project, merge_request, False)


def bz_process_note(gl_instance, message):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    force_eval = common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'bz')
    if not force_eval:
        return

    project = gl_instance.projects.get(message.payload["project"]["id"])
    merge_request = project.mergerequests.get(message.payload["merge_request"]["iid"])
    run_bz_validation(project, merge_request, True)


WEBHOOKS = {
    "merge_request": bz_process_mr,
    "note": bz_process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGZILLA')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
