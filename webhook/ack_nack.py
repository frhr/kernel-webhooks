"""Process all of the ACKs/NACKs that are associated with a merge request."""
import os
import pathlib
import subprocess
import sys

from cki_lib import logger
from cki_lib import misc

from . import common

LOGGER = logger.get_logger('cki.webhook.ack_nack')
MIN_REVIEWERS = 2
MIN_ARK_CONFIG_REVIEWERS = 1
ARK_PROJECT_ID = 13604247

READY_LABEL = ('This merge request has been reviewed by Red Hat engineering and approved for'
               ' inclusion.')
NEEDS_REVIEW_LABEL = 'This merge request needs more reviews and acks from Red Hat engineering.'
NACKED_LABEL = 'This merge request has been explicitly Nacked by Red Hat engineering.'


def _run_get_maintainers(changed_files, linux_src):
    """Run the get_maintainers.pl script."""
    LOGGER.debug('Running get_maintainers.pl in directory %s', linux_src)
    # The get_maintainers.pl script requires the file to exist, so create some
    # empty files as placeholders. It's OK to leave these in place.
    for changed_file in changed_files:
        dirname = os.path.dirname(changed_file)
        os.makedirs(os.path.join(linux_src, os.path.join(dirname)),
                    exist_ok=True)
        pathlib.Path(os.path.join(linux_src, changed_file)).touch()

    os.chdir(linux_src)
    torun = ['./scripts/get_maintainer.pl', '-f'] + changed_files
    ret = subprocess.run(torun, capture_output=True, check=True, text=True)
    LOGGER.debug('Executed %s, return code=%s, stdout=%s',
                 torun, ret.returncode, ret.stdout)

    return (ret.returncode, ret.stdout)


def _save(gl_project, gl_mergerequest, create_gl_status_note, status, message):
    note = f'ACK/NACK Summary: {status} - {message}'
    LOGGER.info(note)

    if status == 'OK':
        label_color = common.READY_LABEL_COLOR
        label_description = READY_LABEL
    else:
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        if status == 'NeedsReview':
            label_description = NEEDS_REVIEW_LABEL
        elif status == 'NACKed':
            label_description = NACKED_LABEL
    label = common.create_label_object(f'Acks::{status}', label_color, label_description)
    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, [label])

    if create_gl_status_note and misc.is_production():
        gl_mergerequest.notes.create({'body': note})


def _get_required_reviewers(changed_files, linux_src):
    """Parse the output from the get_maintainers.pl script and returns list of email addresses."""
    if not changed_files:
        return set([])

    (returncode, stdout) = _run_get_maintainers(changed_files, linux_src)
    if returncode != 0:
        LOGGER.error('Error running get_maintainers.pl (%s): %s', returncode, stdout)
        # Add a bogus required reviewer so that the merge request can't be approved
        return ['ERROR_CHECK_SCRIPT_OUTPUT']

    reviewers = set([])
    for line in stdout.split('\n'):
        if not line:
            continue

        # Subsystem lists have the format like the following and should not be listed as a
        # required reviewer: 'listname@redhat.com (open list:SUBSYSTEM LIST)'
        if '<' in line:
            # First Last <user@redhat.com> (maintainer:SUBSYSTEM)
            reviewers.add(line.split('<')[1].split('>')[0])

    return reviewers


def get_ark_config_mr_ccs(merge_request):
    """Parse any CCs from an ark project MR description."""
    cc_list = []
    if not merge_request.description:
        return cc_list
    mlines = merge_request.description.splitlines()
    for line in mlines:
        if line.startswith('Cc: ') and line.endswith('@redhat.com>'):
            cc_list.append(line.split()[-1].split('<')[1].split('>')[0])
    return cc_list


def get_min_reviewers(project_id, merge_request, files):
    """Return the minimum number of reviews needed for the project."""
    # For ark kernel config changes, also return users in th MR's CC line.
    if project_id == ARK_PROJECT_ID and all(file.startswith('redhat/configs/') for file in files):
        return (MIN_ARK_CONFIG_REVIEWERS, get_ark_config_mr_ccs(merge_request))
    return (MIN_REVIEWERS, [])


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    for tag in ['Acked-by', 'Nacked-by', 'Rescind-acked-by', 'Revoke-acked-by',
                'Rescind-nacked-by', 'Revoke-nacked-by']:
        for line in message.split('\n'):
            search_tag = f'{tag}: '
            if not line.startswith(search_tag):
                continue

            name_email = message.split(search_tag)[1].split(' <')
            if len(name_email) != 2:
                continue

            name = name_email[0]
            email = name_email[1].split('>')[0]

            return (tag, name, email)

    return (None, None, None)


def _process_acks_nacks(messages, last_commit_timestamp, submitter_email):
    # pylint: disable=too-many-branches
    """Process a list of messages and collect any ACKs/NACKS."""
    acks = set([])
    nacks = set([])
    for message_timestamp, message in messages:
        (tag, name, email) = _parse_tag(message)
        if not tag:
            continue

        LOGGER.debug('Processing %s %s <%s> left at %s', tag, name, email, message_timestamp)

        if last_commit_timestamp:
            ack_valid = last_commit_timestamp < message_timestamp
        else:
            ack_valid = True

        if submitter_email == email:
            ack_valid = False

        name_email = (name, email)
        if tag == 'Acked-by' and ack_valid:
            acks.add(name_email)
            if name_email in nacks:
                nacks.remove(name_email)
        elif tag in ('Rescind-acked-by', 'Revoke-acked-by') and ack_valid:
            if name_email in acks:
                acks.remove(name_email)
            else:
                LOGGER.warning('Cannot find ACK to revoke for %s', name_email)
        elif tag == 'Nacked-by':
            nacks.add(name_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by'):
            if name_email in nacks:
                nacks.remove(name_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)

    return (acks, nacks)


def _show_ack_nacks(ack_nacks, required_reviewers):
    num_required_reviewers = 0
    other_reviewers = set(required_reviewers)
    ret = ''

    for idx, ack_nack in enumerate(ack_nacks):
        if idx > 0:
            ret += ', '

        ret += f'{ack_nack[0]} <{ack_nack[1]}>'
        if ack_nack[1] in required_reviewers:
            ret += '★'
            num_required_reviewers += 1
            other_reviewers.remove(ack_nack[1])

    return (ret, num_required_reviewers, other_reviewers)


def _get_ack_nack_summary(acks, nacks, required_reviewers, min_reviewers):
    # pylint: disable=too-many-locals
    footer = ['Code owners are marked with a ★.'] if required_reviewers else []

    (ack_summary, num_required_reviewers, other_reviewers) = _show_ack_nacks(acks,
                                                                             required_reviewers)

    summary = []
    if ack_summary:
        summary.append(f'ACKed by {ack_summary}.')

    (nack_summary, _, _) = _show_ack_nacks(nacks, required_reviewers)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return ('NACKed', ' '.join(summary + footer))

    min_required_reviewers = min(len(required_reviewers), min_reviewers)
    if num_required_reviewers < min_required_reviewers:
        num_missing = min_required_reviewers - num_required_reviewers
        needs_more = f'Requires at least {num_missing} ACK(s) from someone in the set '
        for idx, reviewer in enumerate(other_reviewers):
            if idx > 0:
                needs_more += ', '
            needs_more += f'{reviewer}★'
        needs_more += '.'
        summary.append(needs_more)

        return ('NeedsReview', ' '.join(summary + footer))

    if len(acks) < min_reviewers:
        summary.append(f'Requires {min_reviewers - len(acks)} more ACKs.')

        return ('NeedsReview', ' '.join(summary + footer))

    return ('OK', ' '.join(summary + footer))


def process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src,
                          create_gl_status_note, last_commit_timestamp):
    # pylint: disable=too-many-arguments
    """Process a merge request."""
    changed_files = \
        [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    required_reviewers = _get_required_reviewers(changed_files, linux_src)
    (min_reviewers, cc_reviewers) = get_min_reviewers(gl_project.id, gl_mergerequest, changed_files)
    required_reviewers.update(cc_reviewers)

    notes = [(note.updated_at, note.body)
             for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at', all=True)]

    submitter_email = gl_instance.users.get(gl_mergerequest.author['id']).public_email
    (acks, nacks) = _process_acks_nacks(notes, last_commit_timestamp, submitter_email)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('Minimum reviewers for this MR: %d.', min_reviewers)
    LOGGER.debug('List of possible required reviewers: %s', required_reviewers)
    LOGGER.debug('List of ACKs: %s', acks)
    LOGGER.debug('List of NACKs: %s', nacks)

    summary = _get_ack_nack_summary(acks, nacks, required_reviewers, min_reviewers)
    _save(gl_project, gl_mergerequest, create_gl_status_note, *summary)


def _approve_button_clicked(gl_mergerequest, msg, tag, button_name):
    name_email = '%s <%s>' % (msg.payload['user']['name'], msg.payload['user']['email'])
    body = f'{tag}: {name_email}\n(via {button_name} button)'
    LOGGER.debug('Leaving comment %s', body)

    if misc.is_production():
        gl_mergerequest.notes.create({'body': body})


def _get_last_commit_timestamp(payload):
    if payload.get('merge_request') and payload['merge_request'].get('last_commit'):
        return payload['merge_request']['last_commit'].get('timestamp')
    if payload.get('object_attributes') and payload['object_attributes'].get('last_commit'):
        return payload['object_attributes']['last_commit'].get('timestamp')
    return None


def process_mr_webhook(gl_instance, msg, linux_src):
    """Process a merge request only if a label was changed."""
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['object_attributes']['iid'])

    gl_action = msg.payload['object_attributes'].get('action', '')
    if gl_action == 'approved':
        _approve_button_clicked(gl_mergerequest, msg, 'Acked-by', 'approve')
    elif gl_action == 'unapproved':
        _approve_button_clicked(gl_mergerequest, msg, 'Rescind-acked-by', 'unapprove')

    create_gl_status_note = common.has_label_changed(msg.payload, 'Acks::', True)
    last_commit_timestamp = _get_last_commit_timestamp(msg.payload)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src,
                          create_gl_status_note, last_commit_timestamp)

    return True


def process_note_webhook(gl_instance, msg, linux_src):
    """Process a note message only if a tag was specified."""
    notetext = msg.payload['object_attributes']['note']
    create_gl_status_note = common.force_webhook_evaluation(notetext, 'ack-nack')
    if not _parse_tag(notetext)[0] and not create_gl_status_note:
        LOGGER.debug('Skipping note: %s', notetext)
        return

    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['merge_request']['iid'])
    last_commit_timestamp = _get_last_commit_timestamp(msg.payload)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src,
                          create_gl_status_note, last_commit_timestamp)


WEBHOOKS = {
    'merge_request': process_mr_webhook,
    'note': process_note_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--linux-src', **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory where get_maintainers.pl will be ran')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src)


if __name__ == '__main__':
    main(sys.argv[1:])
