"""Webhook interaction tests."""
import json
import pathlib
import tempfile
import unittest
from unittest import mock

import webhook.common


class MyLister():
    def __init__(self, labels):
        self.labels = labels

    def list(self, search=None, all=False):
        if all:
            return self.labels
        if not search:
            raise(AttributeError)
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(unittest.TestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': '#69D100',
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'Acks::NACKed',
                       'color': '#FF0000',
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NeedsReview',
                       'color': '#FF0000',
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 4,
                       'name': 'Acks::OK',
                       'color': '#FF0000',
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       }]

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = webhook.common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = []
        commits.append("abcdef012345")
        table = []
        table.append(["012345abcdef", commits, 1, "", ""])
        for row in table:
            commits = webhook.common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_extract_files(self):
        """Check function returns expected file list."""

        diff_a = {'new_path': 'net/dev/core.c'}
        diff_b = {'new_path': 'redhat/Makefile'}

        commit = mock.Mock()
        commit.diff.return_value = [diff_a, diff_b]
        self.assertEqual(webhook.common.extract_files(commit),
                         ['net/dev/core.c', 'redhat/Makefile'])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        message = mock.Mock()
        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        message.payload = payload
        self.assertTrue(webhook.common.mr_action_affects_commits(message))
        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        message.payload = payload
        self.assertTrue(webhook.common.mr_action_affects_commits(message))
        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            message.payload = payload
            self.assertFalse(webhook.common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])
        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            message.payload = payload
            self.assertFalse(webhook.common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

    def test_has_label_changed(self):
        msg = {}
        msg['changes'] = {}

        # No 'labels' key
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack', True))

        msg['changes']['labels'] = {}

        # No label changes
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack::', True))

        # Label without prefix changes.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'readyForMerge', False))

        # Label without prefix does not change.
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'readyForMerge'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'readyForMerge'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'readyForMerge', False))

        # Change a label not related to ack/nack
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'}]
        self.assertFalse(webhook.common.has_label_changed(msg, 'ack_nack::', True))

        # Remove ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'ack_nack::OK'},
                                                {'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'somethingelse::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'ack_nack', True))

        # Add ack_nack label
        msg['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg['changes']['labels']['current'] = [{'title': 'ack_nack::OK'},
                                               {'title': 'somethingelse::OK'}]
        self.assertTrue(webhook.common.has_label_changed(msg, 'ack_nack', True))

    def _process_message(self, mock_gl, msg, auth_user):
        webhooks = {'note': mock.Mock()}

        fake_gitlab = mock.Mock(spec=[])

        def mock_auth():
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="mock_auth")

        fake_gitlab.auth = mock_auth
        if auth_user:
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="init")

        mock_gl.return_value.__enter__.return_value = fake_gitlab

        with tempfile.NamedTemporaryFile() as tmp:
            pathlib.Path(tmp.name).write_text(json.dumps(msg, indent=None))
            parser = webhook.common.get_arg_parser('test')
            args = parser.parse_args(['--json-message-file', tmp.name])
            webhook.common.consume_queue_messages(args, webhooks, True)

        if auth_user:
            self.assertEqual(fake_gitlab.user._setup_from, "init")
        else:
            self.assertEqual(fake_gitlab.user._setup_from, "mock_auth")

        return webhooks['note']

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_json_processing(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_no_auth(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, None)
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_bot_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'cki-bot'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_not_called()

    @mock.patch('webhook.common.Message.gl_instance')
    def test_cmdline_regular_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    def test_make_payload(self):
        payload = webhook.common.make_payload('https://web.url/g/p/-/merge_requests/123', 'note')
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload['merge_request']['iid'], 123)

    @mock.patch('json.dumps')
    def test_process_message(self, dumps):
        """Check for expected return value."""
        routing_key = 'FAKE_ROUTE_KEY'
        webhooks = {'merge_request': mock.Mock(), 'note': mock.Mock()}
        payload = {}

        # Wrong object kind should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'delete_project'
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn('Ignoring message from queue', logs.output[-1])

        # Closed state should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'closed'
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn("Ignoring event with 'closed' state.", logs.output[-1])

        # If username matches return False (event was generated by our own activity).
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bot')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'
            payload['user'] = {'username': 'bot'}
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, True))
            payload['user'] = {'username': 'notbot'}
            self.assertTrue(webhook.common.process_message(routing_key, payload, webhooks, True))

        # Ignore notes on issues.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'Issue'}
            self.assertFalse(webhook.common.process_message(routing_key, payload, webhooks, False))
            self.assertIn('Only processing notes related to merge requests', logs.output[-1])

        # Notes on merge requests should be processed.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs, \
             mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'MergeRequest'}
            self.assertTrue(webhook.common.process_message(routing_key, payload, webhooks, False))

    def _test_label_commands(self, mr_labels, new_labels, expected_cmd):
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                label_ret = webhook.common.add_label_to_merge_request(gl_project, 2, new_labels)

                # If label existed on the project, ensure it was edited if needed.
                for label in new_labels:
                    pl_obj = \
                        next((pl for pl in label_objects if pl.name == label['name']), None)
                    pl_dict = \
                        next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']),
                             None)
                    if pl_obj and not (label.items() <= pl_dict.items()):
                        self.assertIn("Editing label %s on project." % label['name'],
                                      ' '.join(logs.output))
                        pl_obj.save.assert_called_once()
                # If new label didn't exist on the project, confirm it was added.
                call_list = []
                for label in new_labels:
                    if not [plabel for plabel in self.PROJECT_LABELS
                            if plabel['name'] == label['name']]:
                        label_string = ("Creating label {'name': '%s', 'color': '%s',"
                                        " 'description': '%s'} on project.") \
                                        % (label['name'], label['color'], label['description'])
                        self.assertIn(label_string, ' '.join(logs.output))
                        call_list.append(mock.call(label))
                if call_list:
                    gl_project.labels.create.assert_has_calls(call_list)
                else:
                    gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label ~"Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label ~"Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'NetSubsystem', 'color': '#123456',
                       'description': 'Net Subsystem label.'}]
        self._test_label_commands(mr_labels, new_labels, '/label ~"NetSubsystem"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label ~"Acks::OK"\n/label ~"readyForMerge"')

        # Add a label which triggers readyForMerge being removed from the Mr.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label ~"Acks::NeedsReview"\n/unlabel ~"readyForMerge"')

        # Add a double scoped label.
        new_labels = [{'name': 'BZ::123::OK', 'color': '#23456',
                       'description': 'Two scopes?'}]
        self._test_label_commands(mr_labels, new_labels, '/label ~"BZ::123::OK"')

        # Add two labels which both need to be created on the project. Use custom colors.
        new_labels = [{'name': 'Label 1', 'color': '#123456', 'description': 'description 1'},
                      {'name': 'Label 2', 'color': '#deadbe', 'description': 'description 2'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label ~"Label 1"\n/label ~"Label 2"')

        # Add a lot of labels to trigger labels.list(all=True)
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Label {count}"
            label = {'name': name, 'color': '#123456', 'description': name}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label ~"Label 1"\n/label ~"Label 2"\n/label ~"Label 3"'
                                   '\n/label ~"Label 4"\n/label ~"Label 5"\n/label ~"Label 6"'
                                   '\n/label ~"Label 7"\n/label ~"Label 8"'))
