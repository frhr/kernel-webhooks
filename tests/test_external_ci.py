"""Tests for external CI module."""
import unittest
from unittest import mock

from tests import fakes
import webhook.common as common
import webhook.external_ci as external_ci


def init_fake_gitlab():
    """Initialize and return a fake gitlab.Gitlab instance."""
    glab = fakes.FakeGitLab()
    glab.add_group('group')
    glab.add_project('path/to/project')
    glab.add_project('outside-group')
    add_members_to = [glab.groups.get('group'),
                      glab.projects.get('path/to/project')]
    for g_or_p in add_members_to:
        g_or_p.members.add_member('testmember')
        g_or_p.members.add_member('anothermember')

    return glab


class TestProcessMessage(unittest.TestCase):
    """Tests for common.process_message()."""

    @mock.patch('webhook.common.Message.gl_instance', mock.Mock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_unrecognized_data(self):
        """Verify unknown message types are ignored."""
        message = {}
        self.assertFalse(
            common.process_message('key', message, external_ci.WEBHOOKS, False)
        )

        message = {'object_kind': 'unknown'}
        self.assertFalse(
            common.process_message('key', message, external_ci.WEBHOOKS, False)
        )

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_pipeline_call(self, mock_gl):
        """Verify correct handler is called for pipeline hook."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {
                       'id': 1,
                       'variables': [{'key': 'origin_path', 'value': 'none'}]
                   },
                   'commit': {'url': 'none'},
                   'state': 'opened'}
        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            common.process_message('key', message, external_ci.WEBHOOKS, False)
            self.assertIn('No matching MR project for none', logs.output[-1])

    @mock.patch('webhook.common.Message.gl_instance', mock.MagicMock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_mr_hook_no_project(self):
        """Verify MR with unknown project is handled correctly."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'unknown'},
                   'state': 'opened'}

        with self.assertLogs('cki.webhook.external_ci', level='ERROR') as logs:
            common.process_message('key', message, external_ci.WEBHOOKS, False)
            self.assertIn('Missing config for', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    def test_mr_hook_group_member(self, mock_gl, mock_config):
        """Check pipelines aren't triggered if the author is a group member."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'group'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message('key', message, external_ci.WEBHOOKS, False)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    def test_mr_hook_project_member(self, mock_gl, mock_config):
        """Test fallback with testing for a project member instead of group."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'anothermember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'path/to/project'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message('key', message, external_ci.WEBHOOKS, False)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_mr_hook_no_code_changes(self, mock_gl, mock_config):
        """Check we abort if the MR code was not modified."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'object_attributes': {'action': 'not-a-code-change'}}
        mock_config.return_value = {'.members_of': 'outside-group'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message('key', message, external_ci.WEBHOOKS, False)
            self.assertIn('Not a code change, ignoring', logs.output[-1])


class TestMain(unittest.TestCase):
    """Sanity tests for external_ci.main()."""

    @mock.patch.dict('os.environ', {'CONFIG_PATH': 'filepath'})
    @mock.patch.dict('webhook.external_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.external_ci.load_config', mock.Mock())
    @mock.patch('webhook.common.get_instance')
    def test_manual(self, mock_gitlab):
        """Verify manual request is correctly handled.

        This includes checking get_manual_hook_data() function.
        """
        args = ['--merge-request',
                'https://gitlab.example/path/to/project/-/merge_requests/11']
        fake_gitlab = init_fake_gitlab()
        fake_project = fake_gitlab.projects.get('path/to/project')
        fake_project.attributes = {
            'name': 'path/to/project',
            'http_url_to_repo': 'https://gitlab.example/path/to/project'
        }
        fake_project.add_mr(
            11,
            {'sha': 'last_commit',
             'author': {'username': 'testmember'},
             'target_branch': 'devel'}
        )
        mock_gitlab.return_value = fake_gitlab

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            external_ci.main(args)
            self.assertIn('Found internal contributor', logs.output[-1])
