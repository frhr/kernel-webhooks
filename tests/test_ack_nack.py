"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.ack_nack
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(unittest.TestCase):
    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_get_required_reviewers(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'somelist@redhat.com (open list:SUBSYSTEM LIST)\n'
        get_maint.return_value = (0, maint_stdout)

        reviewers = webhook.ack_nack._get_required_reviewers(['mocked'], 'mocked')
        self.assertEqual(reviewers, set(['user1@redhat.com', 'user2@redhat.com']))

        reviewers = webhook.ack_nack._get_required_reviewers([], 'mocked')
        self.assertEqual(reviewers, set([]))

    def test_get_ark_config_mr_ccs(self):
        mr = mock.Mock()
        mr.description = ''
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), [])
        mr.description = 'Cool description.\nCc: user1 <user1@redhat.com>\n'
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), ['user1@redhat.com'])

    @mock.patch('webhook.ack_nack.get_ark_config_mr_ccs')
    def test_get_min_reviewers(self, mocked_getarkconfig):
        mocked_mr = mock.Mock()
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = []
        # For non-ark project the function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(12345, mocked_mr, ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that touches non-config files function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi', 'net/core/dev.c'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that only touches config files func should return MIN_ARK_CONFIG_REVIEWERS...
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_ARK_CONFIG_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # ... and if get_ark_config_mr_ccs() returns a list...
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = ['user1@redhat.com']
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_ARK_CONFIG_REVIEWERS)
        self.assertEqual(cc_reviewers, ['user1@redhat.com'])

    def test_parse_tag(self):
        tag = webhook.ack_nack._parse_tag('Acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('LGTM!\n\nAcked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Rescind-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Revoke-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Rescind-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Revoke-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Unknown-tag: User 3 <user3@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('You forgot your Acked-by: User 1 <user1@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('Acked-by: Invalid Format')
        self.assertIsNone(tag[0])

    def test_process_acks_nacks(self):
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 2 <user2@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com'),
                                    ('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Rescind-acked-by: User 1 <user1@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([('User 2', 'user2@redhat.com')]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:52:00.000Z', 'Revoke-nacked-by: User 2 <user2@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Don't allow self ACKs
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Primary email not set on account
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z', None)
        self.assertEqual(acks, set([('User 3', 'user3@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        # This Ack should not be counted since it's before the last_commit_timestamp
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Acked-by: User 2 <user2@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-10T20:00:00.000Z',
                                                             'user3@redhat.com')
        self.assertEqual(acks, set([('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Do not fail if the payload object_attributes does not have a last_commit attribute.
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

    def test_get_ack_nack_summary(self):
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([], [], set([]), 2),
                         ('NeedsReview', 'Requires 2 more ACKs.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com')],
                                                                [], set([]), 2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>. Requires 1 more ACKs.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set([]), 2),
                         ('OK', 'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set(['user1@redhat.com',
                                                                         'user2@redhat.com']), 2),
                         ('OK',
                          'ACKed by User 1 <user1@redhat.com>★, User 2 <user2@redhat.com>★. ' +
                          'Code owners are marked with a ★.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set(['user1@redhat.com',
                                                                         'user3@redhat.com']), 2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>★, User 2 <user2@redhat.com>. ' +
                          'Requires at least 1 ACK(s) from someone in the set ' +
                          'user3@redhat.com★. Code owners are marked with a ★.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [('User 3', 'user3@redhat.com')],
                                                                set(['user1@redhat.com',
                                                                     'user2@redhat.com']), 2),
                         ('NACKed',
                          'ACKed by User 1 <user1@redhat.com>★, User 2 <user2@redhat.com>★. ' +
                          'NACKed by User 3 <user3@redhat.com>. Code owners are marked with a ★.'))

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_process_merge_request(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n'
        get_maint.return_value = (0, maint_stdout)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z'))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z'))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z'))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z'))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z'))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 1}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = []

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []

        gl_instance = mock.Mock()
        gl_instance.users.get.return_value = mock.Mock(public_email='user3@redhat.com')

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   'mocked', True, '2021-01-08T20:00:00.000Z')
            self.assertIn("ACK/NACK Summary: OK - ACKed by ", logs.output[-1])

    def __create_note_body(self, body, timestamp):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        return ret

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message0(self, mocked_process_mr, mock_gl):
        self._test_note("request-ack-nack-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message1(self, mocked_process_mr, mock_gl):
        self._test_note("request-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message2(self, mocked_process_mr, mock_gl):
        self._test_note("Acked-by: User 1 <user1@redhat.com>", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message3(self, mocked_process_mr, mock_gl):
        self._test_note("Some comment", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_not_called()

    def _test_note(self, note_text, mocked_process_mr, mock_gl):
        mock_gl.return_value.__enter__.return_value = mock.Mock()
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = note_text
        self.assertTrue(webhook.common.process_message("ROUTING_KEY", payload,
                                                       webhook.ack_nack.WEBHOOKS,
                                                       False, linux_src='mocked'))

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_other_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Change a label not related to ack/nack
        msg.payload['changes']['labels']['previous'] = [{'title': 'Acks::OK'},
                                                        {'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'}]

        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, 'mocked',
                                           False, '2021-01-08T20:00:00.000Z')

        # Process an event with no last_commit attribute.
        msg.payload['object_attributes'].pop('last_commit')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, 'mocked',
                                           False, None)

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_ack_nack_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Add ack_nack label
        msg.payload['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'},
                                                       {'title': 'somethingelse::OK'}]

        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, 'mocked', True,
                                           '2021-01-08T20:00:00.000Z')

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1, 'web_url': 'https://web.url/g/p'},
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    @mock.patch('webhook.ack_nack.process_merge_request', mock.Mock())
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_approve_button(self):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_mergerequest.notes = mock.Mock()
        gl_mergerequest.notes.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('approved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': 'Acked-by: User 1 <user1@redhat.com>\n(via approve button)'})

        msg.payload = self._create_merge_payload('unapproved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': 'Rescind-acked-by: User 1 <user1@redhat.com>\n(via unapprove button)'})
