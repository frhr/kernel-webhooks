"""Webhook interaction tests."""
import copy
import json
import os
import unittest
from unittest import mock

import webhook.bugzilla
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBugzilla(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'state': 'opened'
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    BZ_RESULTS = [{'id': 1234567,
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'external_bugs': []
                   }]

    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(webhook.bugzilla.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.bugzilla.connect_bugzilla', return_value=True):
            self.assertTrue(webhook.bugzilla.try_bugzilla_conn())

    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(webhook.bugzilla.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            self.assertFalse(webhook.bugzilla.connect_bugzilla(api_key))
            self.assertTrue(logs[-1][0].startswith(('ERROR:cki.webhook.bugzilla:'
                                                    'Problem connecting to bugzilla server.')))
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            self.assertFalse(webhook.bugzilla.connect_bugzilla(api_key))
            self.assertTrue(logs[-1][0].startswith(('ERROR:cki.webhook.bugzilla:'
                                                    'Problem with file permissions')))

    @mock.patch('webhook.bugzilla.add_mr_to_bz', return_value=True)
    def test_update_bugzilla(self, mocked_addmrtobz):
        """Check for expected calls."""
        project = mock.Mock()
        merge_request = mock.Mock()
        merge_request.iid = 10
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            webhook.bugzilla.update_bugzilla(project, [], merge_request)
            self.assertEqual(logs[-1][0], 'DEBUG:cki.webhook.bugzilla:Input bug list is empty.')
        with self.assertLogs('cki.webhook.bugzilla', level='INFO') as logs:
            webhook.bugzilla.update_bugzilla(project, ['INTERNAL'], merge_request)
            self.assertIn('bugzilla actions: [\'INTERNAL\']', logs[-1][0])
            mocked_addmrtobz.assert_not_called()
        bug_list = ['252626', '739272', '1544362', 'INTERNAL']
        with mock.patch('webhook.bugzilla.try_bugzilla_conn', return_value=True):
            with self.assertLogs('cki.webhook.bugzilla', level='INFO') as logs:
                webhook.bugzilla.update_bugzilla(project, bug_list, merge_request)
                self.assertIn('bugzilla actions: [\'INTERNAL\']', logs[-1][0])
                mocked_addmrtobz.assert_called_with(True, project,
                                                    ['252626', '739272', '1544362'], 10)

    def test_bz_is_linked_to_mr(self):
        """Check for expected return values."""
        domain = 'gitlab.com'
        path = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        self.assertTrue(webhook.bugzilla.bz_is_linked_to_mr(bz0, domain, path))
        self.assertFalse(webhook.bugzilla.bz_is_linked_to_mr(bz1, domain, path))
        self.assertFalse(webhook.bugzilla.bz_is_linked_to_mr(bz2, domain, path))

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_add_mr_to_bz(self):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        project = mock.Mock()
        project.web_url = 'https://gitlab.com/redhat/rhel/8.y/kernel'
        project.path_with_namespace = 'redhat/rhel/8.y/kernel'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        bzcon = mock.Mock()
        merge_request = 10

        # Early return due to empty bug_list
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            webhook.bugzilla.add_mr_to_bz(bzcon, project, [], merge_request)
            self.assertEqual(('ERROR:cki.webhook.bugzilla:MR %d has no bugs? '
                              'Skipping linking of bugs to MR.' % merge_request), logs.output[-1])

        # Early return due to no untracked bugs
        bzcon.getbugs.return_value = [bz0]
        with self.assertLogs('cki.webhook.bugzilla', level='INFO') as logs:
            webhook.bugzilla.add_mr_to_bz(bzcon, project, [bz0.id], merge_request)
            self.assertEqual(('INFO:cki.webhook.bugzilla:All bugs have an '
                              'existing link to MR %d.' % merge_request), logs.output[-1])

        # Successful call to add_external_tracker()
        bzcon.getbugs.return_value = [bz0, bz1, bz2]
        bzcon.add_external_tracker.return_value = True
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            webhook.bugzilla.add_mr_to_bz(bzcon, project, [bz0.id, bz1.id, bz2.id], merge_request)
        ext_type_url = 'https://gitlab.com/'
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon.add_external_tracker.assert_called_once_with([8675309, 66442200],
                                                           ext_type_url=ext_type_url,
                                                           ext_bz_bug_id=ext_bz_bug_id)

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs = webhook.bugzilla.extract_dependencies(None)
        self.assertEqual(bzs, [])

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n')
        bzs = webhook.bugzilla.extract_dependencies(message)
        self.assertEqual(bzs, ['22334455', '33445566'])

    def test_extract_all_bzs(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs = webhook.bugzilla.extract_all_bzs(None, [], [])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456'
        bzs, non_mr_bzs, dep_bzs = webhook.bugzilla.extract_all_bzs(message1, [], [])
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        bzs, non_mr_bzs, dep_bzs = webhook.bugzilla.extract_all_bzs(message1, [], ['18123456'])
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs = webhook.bugzilla.extract_all_bzs(message1, ['12345678'], [])
            self.assertIn(('Bugzilla: 18123456 not listed in MR description.'), logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'BUGZILLA_ROUTING_KEYS': 'mocked', 'BUGZILLA_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        properties = mock.Mock()
        headers = {'header-key': 'value'}
        setattr(properties, 'headers', headers)
        consume_value = [(mock.Mock(), properties, json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.bugzilla.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload, False)

    def test_validate_internal_bz(self):
        """Check for expected return value."""
        commits = {"deadb": ['redhat/Makefile']}
        self.assertTrue(webhook.bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h']
        self.assertFalse(webhook.bugzilla.validate_internal_bz(commits))

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab):
        """Check handling of a note."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE, False)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation1(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation2(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    def test_get_dependencies_label_scope(self):
        """Test that a Dependencies label gets set properly."""
        scope = webhook.bugzilla.get_dependencies_label_scope(True, "1234567890abcdef")
        self.assertEqual("1234567890ab", scope)
        scope = webhook.bugzilla.get_dependencies_label_scope(False, "1234567890abcdef")
        self.assertEqual("OK", scope)

    def _test_payload(self, mocked_gitlab, result, payload, assert_gitlab_called):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)

    # pylint: disable=too-many-arguments,too-many-locals
    @mock.patch('webhook.bugzilla.update_bugzilla')
    def _test(self, mocked_gitlab, result, payload, assert_gitlab_called, mocked_bugzilla):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.MagicMock(target_branch=target)
        c1 = mock.Mock(id="1234",
                       message="1\n"
                       "Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567",
                       message="2\nBugzilla: http://bugzilla.test?id=45678\n"
                       "Bugzilla: https://bugzilla.redhat.com/4567\n"
                       "Depends: http://bugzilla.redhat.com/1234",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="890a",
                       message="3\nBugzilla: INTERNAL",
                       parent_ids=["face"])
        c3.diff = mock.Mock(return_value=[{'new_path': 'redhat/Makefile'}])
        c4 = mock.Mock(id="face",
                       message="This is a merge",
                       parent_ids=["abcd", "4567"])
        c5 = mock.Mock(id="deadbeef",
                       message="5\nBugzilla: 565472, 7784378\n"
                       "Nothing to see here\n"
                       "Bugzilla: http://bugzilla.redhat.com/1234567\n"
                       "Bugzilla: https://bugzilla.redhat.com/7654321\n"
                       "Bugzilla: http://bugzilla.redhat.com/1989898",
                       parent_ids=["890a"])
        c6 = mock.Mock(id="1800555",
                       message="6\nBugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1800555",
                       parent_ids=["deadbeef"])
        c7 = mock.Mock(id="e404",
                       message="7\n"
                       "https://bugzilla.redhat.com/show_bug.cgi?id=9983023",
                       parent_ids=["1800555"])
        c8 = mock.Mock(id="abcd",
                       message="8\n"
                       "Depends: "
                       "https://bugzilla.redhat.com/show_bug.cgi?id=7654321\n",
                       parent_ids=["1800555"])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.iid = 2
        merge_request.labels = []
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6, c7, c8]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "face": c4,
                           "deadbeef": c5, "1800555": c6, "e404": c7,
                           "abcd": c8}
        self.assertEqual(webhook.bugzilla.extract_bzs(c1.message), ["1234"])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c1.message), [])
        self.assertEqual(webhook.bugzilla.extract_bzs(c2.message), ["4567"])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c2.message),
                         ["1234"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c3.message), ["INTERNAL"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c5.message),
                         ["1234567", "7654321", "1989898"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c6.message), ["1800555"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c7.message), [])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c8.message),
                         ["7654321"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No approved issue IDs referenced in log message or"
                     " changelog for HEAD\n*** Unapproved issue IDs referenced"
                     " in log message or changelog for HEAD\n*** Commit HEAD"
                     " denied\n*** Current checkin policy requires:\n"
                     "    release == +\n"
                     "*** See https://rules.doc/doc for more information",
            "result": "fail",
            "logs": "*** Checking commit HEAD\n*** Resolves:\n***   "
                    "Unapproved:\n***     rhbz#1234 (release?, qa_ack?, "
                    "devel_ack?, mirror+)\n"
        }

        with mock.patch('webhook.bugzilla.SESSION.post', return_value=presult):
            return_value = \
                webhook.common.process_message('dummy', payload,
                                               webhook.bugzilla.WEBHOOKS, False)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)

            self.assertTrue(return_value)
            if assert_gitlab_called:
                mocked_bugzilla.assert_called_once_with(project, [], merge_request)
                mocked_gitlab().__enter__().projects.get.assert_called_with(1)
                project.mergerequests.get.assert_called_with(2)
            else:
                mocked_gitlab().__enter__().projects.get.assert_not_called()
                project.mergerequests.get.assert_not_called()
        else:
            self.assertFalse(return_value)
