"""Webhook interaction tests."""
import unittest
from unittest import mock

import webhook.common
import webhook.subsystems


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestSubsystems(unittest.TestCase):

    MAPPINGS = {'driver ixgbe': ['drivers/net/ethernet/intel/ixgbe/'],
                'include': ['include/'],
                'mm': ['arch/x86/mm/', 'include/linux/mm.h', 'mm/'],
                'net': ['include/linux/net.h', 'include/net/', 'net/']
                }

    FILE_CONTENTS = (
        '---\n'
        'driver ixgbe:\n'
        '  - drivers/net/ethernet/intel/ixgbe/\n'
        'include:\n'
        '  - include/\n'
        'mm:\n'
        '  - arch/x86/mm/\n'
        '  - include/linux/mm.h\n'
        '  - mm/\n'
        'net:\n'
        '  - include/linux/net.h\n'
        '  - include/net/\n'
        '  - net/\n'
        )

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            result = webhook.subsystems.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = webhook.subsystems.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_get_subsystems(self):
        result = webhook.subsystems.get_subsystems('net/core/dev.c', self.MAPPINGS)
        self.assertEqual(result, ['net'])
        result = webhook.subsystems.get_subsystems('include/net/ip.h', self.MAPPINGS)
        self.assertEqual(sorted(result), ['include', 'net'])
        result = webhook.subsystems.get_subsystems('weirdfile.bat', self.MAPPINGS)
        self.assertEqual(result, [])

    def test_make_labels(self):
        topics = ['driver ixgbe', 'include', 'net']
        expected_list = [{'name': f'{webhook.subsystems.DRIVER_LABEL_PREFIX}:ixgbe',
                          'color': webhook.subsystems.DRIVER_LABEL_COLOR,
                          'description': webhook.subsystems.DRIVER_LABEL_DESC % 'ixgbe'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:include',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'include'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'net'}
                         ]

        result = webhook.subsystems.make_labels(topics)
        self.assertEqual(len(result), 3)
        self.assertEqual(result, expected_list)

    @mock.patch('webhook.common.extract_files')
    def test_do_mapping(self, mock_extract_files):
        mock_mr = mock.Mock()
        mock_mr.commits.return_value = [1]

        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            mock_extract_files.return_value = ['net/core/dev.c', 'include/net/ip.h']
            result = webhook.subsystems.do_mapping(mock_mr, self.MAPPINGS)
            self.assertEqual(len(result), 2)
            result_names = sorted([label['name'] for label in result])
            self.assertEqual(result_names[0], 'Subsystem:include')
            self.assertEqual(result_names[1], 'Subsystem:net')
            self.assertIn("Path 'net/core/dev.c' matched topics: ['net']",
                          ''.join(logs.output))
            self.assertIn("Path 'include/net/ip.h' matched topics: ['include', 'net']",
                          ''.join(logs.output))

        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            mock_extract_files.return_value = ['net/core/dev.c', 'include/net/ip.h',
                                               'drivers/net/ethernet/intel/ixgbe/ixgbe_main.c']
            result = webhook.subsystems.do_mapping(mock_mr, self.MAPPINGS)
            self.assertEqual(len(result), 3)
            result_names = sorted([label['name'] for label in result])
            self.assertEqual(result_names[0], 'Driver:ixgbe')
            self.assertEqual(result_names[1], 'Subsystem:include')
            self.assertEqual(result_names[2], 'Subsystem:net')
            self.assertIn("Path 'net/core/dev.c' matched topics: ['net']",
                          ''.join(logs.output))
            self.assertIn("Path 'include/net/ip.h' matched topics: ['include', 'net']",
                          ''.join(logs.output))

        with self.assertLogs('cki.webhook.subsystems', level='DEBUG') as logs:
            mock_extract_files.return_value = ['weirdfile.bat']
            result = webhook.subsystems.do_mapping(mock_mr, self.MAPPINGS)
            self.assertEqual(result, None)
            self.assertIn("No matching subsystem topics found for path 'weirdfile.bat'",
                          ''.join(logs.output))
            self.assertIn("No matching subsystem topics found for MR file list: {'weirdfile.bat'}",
                          ''.join(logs.output))

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.common.create_label_object')
    @mock.patch('webhook.common.extract_files')
    @mock.patch('webhook.subsystems.load_yaml_data')
    def test_process_mr(self, mock_loader, mock_extract_files, mock_create_label,
                        mock_add_label, mock_gl):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.commits.return_value = [1]
        mock_project = mock.Mock()
        mock_gl.projects.get.return_value = mock_project
        mock_project.mergerequests.get.return_value = mock_mr

        msg = mock.Mock()
        msg.payload = {}
        msg.payload['project'] = {'id': 1}
        msg.payload['changes'] = {'labels': {'previous': [],
                                             'current': [{'title': 'Subsystem:'}]
                                             }
                                  }
        msg.payload['object_attributes'] = {'iid': 2, 'action': 'open'}

        # No yaml data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_loader.return_value = None
            webhook.subsystems.process_mr(mock_gl, msg, 'bad_map_file.yml')
            self.assertIn("Mapping data not loaded from 'bad_map_file.yml'", logs.output[-1])
            mock_create_label.assert_not_called()
            mock_add_label.assert_not_called()

        # No label to add.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_loader.return_value = self.MAPPINGS
            mock_extract_files.return_value = ['weirdfile.bat']
            webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml')
            self.assertIn('No labels to add.', logs.output[-1])
            mock_create_label.assert_not_called()
            mock_add_label.assert_not_called()

        # Label.
        mock_extract_files.return_value = ['net/core/dev.c']
        name = f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net'
        color = webhook.subsystems.SUBSYS_LABEL_COLOR
        desc = webhook.subsystems.SUBSYS_LABEL_DESC % 'net'
        label_dict = {'name': name, 'color': color, 'description': desc}
        mock_create_label.return_value = label_dict
        webhook.subsystems.process_mr(mock_gl, msg, 'map_file.yml')
        mock_create_label.assert_called_with(name, color, desc)
        mock_add_label.assert_called_with(mock_project, 2, [label_dict])
